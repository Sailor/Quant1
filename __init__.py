from datetime import date, datetime, timedelta
from decorator import decorator
from enum import IntEnum
import os
from typing import Tuple

ROOT_FOLDER = os.path.expanduser('~/.trading')
EXTENSION = 'csv'
SEPARATOR = ';'
MARKETS = ('stocks', 'options', 'currencies', 'crypto', 'etf')
LOG_LEVELS = IntEnum('LOG_LEVELS', ['WARNING', 'ERROR'])
INTERVALS = ('1W', '1D', '1H', '5M', '1M')
DEFAULT_SOURCE = 'yfinance'
DATE_FORMAT = '%Y%m%d'
DATE_FORMAT_DASH = '%Y-%m-%d'
GLOBAL_START = '20000101'
MIN_INTERVAL_1D = 58
MIN_INTERVAL_1H = 4
COUNT_FILE_WARNING = 3
SPAN_1_DAY = timedelta(days=1)
SPAN_5_DAYS = timedelta(days=5)
NEXT_WORKING_DAY = (1, 1, 1, 1, 3, 2, 1)  # monday is 0
PREV_WORKING_DAY = (3, 1, 1, 1, 1, 1, 2)

CLEAN_TARGETS = IntEnum('CLEAN_TARGETS', ['ALL', 'CACHE', 'OTHER'])
# all files, all cache files, all cache files other than current cache file


def get_pure_name(file) -> str:
    return os.path.splitext(os.path.basename(file))[0]


def get_dates(file, clean=False) -> Tuple[str, str]:
    """ Extract start and end dates from file name
    :param clean: if True, remove the eventual trailing underscore of file name
    """
    name = get_pure_name(file)
    if clean and name.endswith('_'):
        name = name[:-1]
    return name.split('-')


def is_refresh_end(file) -> bool:
    return os.path.splitext(file)[0].endswith('_')


class FileMixin:
    def get_dates(self):
        return get_dates(self)

    def is_refresh_end(self):
        return is_refresh_end(self)


class FileName(FileMixin, str):
    """ A simple file name like 'toto.csv'
    """
    def to_path(self, path):
        return PathName(os.path.join(path, self))


class PathName(FileMixin, str):
    """ An complete absolute pathname
    """
    def to_file(self):
        return FileName(os.path.basename(self))

    @classmethod
    def from_components(cls, path, file):
        return cls(os.path.join(path, file))


def str2date(_date: str) -> date:
    return datetime.strptime(_date, DATE_FORMAT).date()


@decorator
def date_deco(func, *args):
    """ if 1st param is a str, return a str else return a date
    """
    d = args[0] if args else datetime.utcnow().date()
    date_is_str = isinstance(d, str)
    d = str2date(d) if date_is_str else d
    res = func(d)
    return res.strftime(DATE_FORMAT) if date_is_str else res


@date_deco
def next_work_date(*args) -> date:
    d = args[0]
    return d + timedelta(days=NEXT_WORKING_DAY[d.weekday()])


@date_deco
def prev_work_date(*args) -> date:
    d = args[0]
    return d - timedelta(days=PREV_WORKING_DAY[d.weekday()])


def date_f(date: str) -> str:
    """ Reformat string date from YYYYMMDD to YYYY-MM-DD
    """
    return '-'.join((date[:4], date[4:6], date[6:]))


def truncate_tail(file: PathName):
    with open(file, 'rb+') as f:
        f.seek(0)
        cont = f.read()
        idx = cont.rfind(b'\n', 0, -1) + 1
        f.seek(idx)
        f.truncate()


def append_file(file1: PathName, file2: PathName, f1_refresh_tail=False, f2_jump_head=True):
    """
    Append file2 to file1:
    - first append CR at end of file1 if missing,
    - optionally jumps first line of file2 (jump CSV headers)
    - optionally overwrite last line of file1 (refresh last line)
    """
    with open(file1, 'rb+') as f1, open(file2, 'rb') as f2:
        f1.seek(-1, 2)
        last_char = f1.read()
        if last_char != b'\n':
            f1.write(b'\n')
        if f1_refresh_tail:
            f1.seek(0)
            cont = f1.read()
            idx = cont.rfind(b'\n', 0, -1) + 1
            f1.seek(idx)
            f1.truncate()
        if f2_jump_head:
            f2.readline()
        f1.write(f2.read())
