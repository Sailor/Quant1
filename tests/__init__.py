from decorator import decorator
import os
import shutil

TEST_TICKER = "TEST"


@decorator
def clean_folder(func, *args):
    """ Test case deco that cleans all the mess after a test
    """
    try:
        func(*args)
    finally:
        shutil.rmtree(os.path.expanduser(f'~/.trading/stocks/{TEST_TICKER}'), ignore_errors=True)


def touch(file, content=None):
    """ Create or erase a file (like unix touch command)
    """
    with open(file, 'w') as f:
        if content:
            f.write(content)


class SourceMock:
    def __init__(self, value):
        self.value = value
        self.count = 0
        self.call_params = None
        self.f_params = []

    def __call__(self, *args):
        self.call_params = args
        return self

    def function(self, *args, **kwargs):
        self.f_params.append(dict(args=args, kwargs=kwargs))
        value = self.value
        if isinstance(value, tuple):
            value = value[self.count]
            self.count += 1
        if callable(value):
            value = value()
        if isinstance(value, BaseException):
            raise value
        return value
