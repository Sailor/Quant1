from datetime import date, datetime, timedelta
from freezegun import freeze_time

from .. import is_refresh_end, next_work_date, prev_work_date


def test_is_refresh_end():
    assert not is_refresh_end('a.txt')
    assert not is_refresh_end('a/b/c.txt')
    assert not is_refresh_end('/a/b/c.txt')
    assert is_refresh_end('a_.txt')
    assert is_refresh_end('a/b/c_.txt')
    assert is_refresh_end('/a/b/c_.txt')


def test_next_work_day():
    result = (1, 2, 3, 4, 0, 0, 0)
    for d in range(1, 8):
        day = date(2022, 11, d)  # from tuesday 1 nov to monday 7 nov
        assert day.weekday() == d % 7
        assert next_work_date(day).weekday() == result[day.weekday()]


def test_prev_work_day():
    result = (4, 0, 1, 2, 3, 4, 4)
    for d in range(1, 8):
        day = date(2022, 11, d)  # idem
        assert prev_work_date(day).weekday() == result[day.weekday()]


@freeze_time("2022-12-01")
def test_next_work_date_types():
    assert next_work_date() == datetime.utcnow().date() + timedelta(days=1)
    assert next_work_date("20221201") == "20221202"  # Thursday -> Friday
    assert next_work_date("20221202") == "20221205"  # Friday -> Monday
    assert next_work_date(date(2022, 12, 2)) == date(2022, 12, 5)  # Friday -> Monday
