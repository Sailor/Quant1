
from ..main import Manager
from ..sources import make_csv
from .mock_yfinance import *
from . import TEST_TICKER, clean_folder

"""
Here we check that the files contents are correct:
- There is no duplicate line
- There is no missing line
- Each other rule is respected
"""


@clean_folder
def test_cache_append_one_day():
    t = Manager().get_ticker(TEST_TICKER, start='20221114', end='20221115')
    make_csv(MOCK_MONDAY14_2DAYS, '1D', t.get_path())
    assert t.get_files() == ['20221114-20221115.csv']
    data = t.get_raw_data()
    assert data.shape == (2, 5)
    t = Manager().get_ticker(TEST_TICKER, start='20221114', end='20221116')
    with mock_patch(MOCK_WEDNESDAY16_1DAY) as mocker:
        data = t.get_raw_data()
        assert mocker.f_params[0]['kwargs'] == {'start': '2022-11-16', 'end': '2022-11-17', 'interval': '1d'}
    assert data.shape == (3, 5)
    assert t.get_files() == ['20221114-20221116.csv']


@clean_folder
def test_cache_append_marked():
    t = Manager().get_ticker(TEST_TICKER, start='20221114', end='20221115')
    make_csv(MOCK_MONDAY14_2DAYS, '1D', t.get_path(mark=True))
    assert t.get_files() == ['20221114-20221115_.csv']
    data = t.get_raw_data()
    assert data.shape == (2, 5)
    t = Manager().get_ticker(TEST_TICKER, start='20221114', end='20221116')
    with mock_patch(MOCK_WEDNESDAY16_1DAY) as mocker:
        data = t.get_raw_data()
        assert mocker.f_params[0]['kwargs'] == {'start': '2022-11-15', 'end': '2022-11-17', 'interval': '1d'}
    assert data.shape == (2, 5)
    assert t.get_files() == ['20221114-20221116.csv']


@clean_folder
def test_cache_append_over_we():
    t = Manager().get_ticker(TEST_TICKER, start='20221118', end='20221118')
    make_csv(MOCK_FRIDAY18_1DAY, '1D', t.get_path())
    assert t.get_files() == ['20221118-20221118.csv']
    data = t.get_raw_data()
    assert data.shape == (1, 5)
    t = Manager().get_ticker(TEST_TICKER, start='20221118', end='20221121')
    with mock_patch(MOCK_MONDAY21_1DAY) as mocker:
        data = t.get_raw_data()
        assert mocker.f_params[0]['kwargs'] == {'start': '2022-11-21', 'end': '2022-11-22', 'interval': '1d'}
    assert data.shape == (2, 5)
    assert t.get_files() == ['20221118-20221121.csv']


@clean_folder
def test_cache_append_over_we_marked():
    t = Manager().get_ticker(TEST_TICKER, start='20221118', end='20221118')
    make_csv(MOCK_FRIDAY18_1DAY, '1D', t.get_path(mark=True))
    assert t.get_files() == ['20221118-20221118_.csv']
    data = t.get_raw_data()
    assert data.shape == (1, 5)
    t = Manager().get_ticker(TEST_TICKER, start='20221118', end='20221121')
    with mock_patch(MOCK_MONDAY21_1DAY) as mocker:
        data = t.get_raw_data()
        assert mocker.f_params[0]['kwargs'] == {'start': '2022-11-18', 'end': '2022-11-22', 'interval': '1d'}
    assert data.shape == (1, 5)
    assert t.get_files() == ['20221118-20221121.csv']


@clean_folder
def test_cache_filter():
    t = Manager().get_ticker(TEST_TICKER, start='20221114', end='20221115')
    make_csv(MOCK_MONDAY14_2DAYS, '1D', t.get_path())
    assert t.get_files() == ['20221114-20221115.csv']
    data = t.get_data()
    assert data.shape == (2, 5)
    t = Manager().get_ticker(TEST_TICKER, start='20221115', end='20221115')
    data = t.get_data()
    assert data.shape == (1, 5)
