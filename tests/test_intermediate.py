from freezegun import freeze_time

from ..main import Manager
from . import TEST_TICKER, clean_folder, touch


@clean_folder
@freeze_time("2022-03-12")
def test_get_from_cache_empty():
    t = Manager().get_ticker(TEST_TICKER)
    assert t.get_from_cache() == [(None, "20220101", "20220312", '')]
    touch(t.get_path("toto"))
    assert t.get_from_cache() == [(None, "20220101", "20220312", '')]


@clean_folder
@freeze_time("2022-03-12")
def test_get_from_cache_file_past():
    t = Manager().get_ticker(TEST_TICKER)
    touch(t.get_path("20210101-20210201.csv"))
    assert t.get_from_cache() == [(None, "20220101", "20220312", '')]


@clean_folder
@freeze_time("2022-03-12")
def test_get_from_cache_partial_left():
    t = Manager().get_ticker(TEST_TICKER)
    touch(t.get_path("20211201-20220201.csv"))
    assert t.get_from_cache() == [("20211201-20220201.csv", "20220201", "20220312", 'a')]


@clean_folder
@freeze_time("2022-03-12")
def test_get_from_cache_partial_right():
    t = Manager().get_ticker(TEST_TICKER)
    touch(t.get_path("20220201-20220401.csv"))
    assert t.get_from_cache() == [("20220201-20220401.csv", "20220101", "20220201", 'p')]


@clean_folder
@freeze_time("2022-03-12")
def test_get_from_cache_complete():
    t = Manager().get_ticker(TEST_TICKER)
    touch(t.get_path("20220101-20220401.csv"))
    assert t.get_from_cache() == [("20220101-20220401.csv", None, None, '')]


@clean_folder
@freeze_time("2022-03-12")
def test_get_from_cache_inside():
    t = Manager().get_ticker(TEST_TICKER)
    touch(t.get_path("20220201-20220301.csv"))
    assert t.get_from_cache() == [("20220201-20220301.csv", "20220101", "20220201", 'p'),
                                  (None, "20220301", "20220312", 'a')]


@clean_folder
@freeze_time("2022-03-12")
def test_get_from_cache_split(capsys):
    t = Manager().get_ticker(TEST_TICKER)
    t.m.count_warning = 2
    touch(t.get_path("20220101-20220201.csv"))
    touch(t.get_path("20220301-20220401.csv"))
    assert t.get_from_cache() == [("20220101-20220201.csv", "20220201", "20220301", 'a'),
                                  ("20220301-20220401.csv", None, None, 'p')]
    captured = capsys.readouterr()
    assert captured.out == f"WARNING: {t.m.path}/TEST/1D contains 2 csv files\n"
