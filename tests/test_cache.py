from freezegun import freeze_time

from ..main import Manager
from .mock_yfinance import SIMPLE_MOCK, mock_patch
from . import TEST_TICKER, clean_folder, touch

"""
Here we check that the files are correctly managed and named
"""


HEADERS = "Date;Open;High;Low;Close;Volume\n"
CONTENT = "2022-11-10;36.07;36.2;34.31;34.41;13179300\n"
HEADERS_CONTENT = HEADERS + CONTENT


@clean_folder
@mock_patch(SIMPLE_MOCK)
@freeze_time("2022-03-12")
def test_cache_empty():
    # Case 1
    t = Manager().get_ticker(TEST_TICKER)
    assert t.get_files() == []
    assert t.file is None
    data = t.get_raw_data()
    assert data.shape == (2, 5)
    file = "20220101-20220312_.csv"
    assert t.end_is_today
    assert t.get_file(mark=True) == file
    assert t.get_files() == [file]
    assert t.file.to_file() == file
    data2 = t.read_file()
    assert data.equals(data2)


@clean_folder
@mock_patch(SIMPLE_MOCK)
@freeze_time("2022-03-12")
def test_cache_miss():
    # Case 1 bis
    t = Manager().get_ticker(TEST_TICKER)
    file = t.get_path("20210101-20210201.csv")
    touch(file, content=HEADERS)
    data = t.get_raw_data()
    assert data.shape == (2, 5)
    new_file = t.get_file(mark=True)
    assert new_file == "20220101-20220312_.csv"
    assert t.get_files() == ["20210101-20210201.csv", new_file]
    assert t.file.to_file() == new_file
    data2 = t.read_file()
    assert data.equals(data2)


@clean_folder
@mock_patch(SIMPLE_MOCK)
@freeze_time("2022-03-12")
def test_cache_partial_left():
    # Case 3
    t = Manager().get_ticker(TEST_TICKER)
    left = t.get_path("20211201-20220201.csv")
    touch(left, content=HEADERS)
    data = t.get_raw_data()
    assert data.shape == (2, 5)
    assert t.get_files() == ["20211201-20220312_.csv"]
    assert t.file.to_file() == "20211201-20220312_.csv"
    data2 = t.read_file()
    assert data.equals(data2)


@clean_folder
@mock_patch(SIMPLE_MOCK)
@freeze_time("2022-03-12")
def test_cache_complete_left():
    # Case 3
    t = Manager().get_ticker(TEST_TICKER)
    left = t.get_path("20220101-20220201.csv")
    touch(left, content=HEADERS)
    data = t.get_raw_data()
    assert data.shape == (2, 5)
    assert t.get_files() == ["20220101-20220312_.csv"]
    assert t.file.to_file() == "20220101-20220312_.csv"
    data2 = t.read_file()
    assert data.equals(data2)


@clean_folder
@mock_patch(SIMPLE_MOCK)
@freeze_time("2022-04-12")  # "today"
def test_cache_partial_right():
    # Case 4
    t = Manager().get_ticker(TEST_TICKER, start='20220101', end='20220301')
    right = t.get_path("20220201-20220401.csv")
    touch(right, content=HEADERS)
    data = t.get_raw_data()
    assert data.shape == (2, 5)
    assert t.get_files() == ["20220101-20220401.csv"]
    assert t.file.to_file() == "20220101-20220401.csv"
    data2 = t.read_file()
    assert data.equals(data2)


@clean_folder
@mock_patch(SIMPLE_MOCK)
@freeze_time("2022-03-12")
def test_cache_complete_right():
    # Case 4
    t = Manager().get_ticker(TEST_TICKER)
    right = t.get_path("20220201-20220312_.csv")
    touch(right, content=HEADERS)
    data = t.get_raw_data()
    assert data.shape == (2, 5)
    assert t.get_files() == ["20220101-20220312_.csv"]
    assert t.file.to_file() == "20220101-20220312_.csv"
    data2 = t.read_file()
    assert data.equals(data2)


@clean_folder
@freeze_time("2022-04-12")  # "today"
def test_cache_complete():
    # Case 2
    t = Manager().get_ticker(TEST_TICKER, start='20220101', end='20220301')
    file = t.get_path("20211201-20220401.csv")
    touch(file, content=HEADERS_CONTENT)
    data = t.get_raw_data()
    assert data.shape == (1, 5)
    assert t.get_files() == ["20211201-20220401.csv"]
    assert t.file.to_file() == "20211201-20220401.csv"
    data2 = t.read_file()
    assert data.equals(data2)


@clean_folder
@freeze_time("2022-03-12")
def test_cache_exact():
    # Case 2
    t = Manager().get_ticker(TEST_TICKER)
    file = t.get_path("20220101-20220312_.csv")
    touch(file, content=HEADERS_CONTENT)
    data = t.get_raw_data()
    assert data.shape == (1, 5)
    assert t.get_files() == ["20220101-20220312_.csv"]
    assert t.file.to_file() == "20220101-20220312_.csv"
    data2 = t.read_file()
    assert data.equals(data2)


@clean_folder
@mock_patch((SIMPLE_MOCK, SIMPLE_MOCK))
@freeze_time("2022-03-12")
def test_cache_inside():
    # Case 5
    t = Manager().get_ticker(TEST_TICKER)
    file = t.get_path("20220201-20220301.csv")
    touch(file, content=HEADERS_CONTENT)
    data = t.get_raw_data()
    assert data.shape == (5, 5)
    assert t.get_files() == ["20220101-20220312_.csv"]
    assert t.file.to_file() == "20220101-20220312_.csv"
    data2 = t.read_file()
    assert data.equals(data2)


@clean_folder
@mock_patch(SIMPLE_MOCK)
@freeze_time("2022-04-12")  # "today"
def test_cache_partial_left_and_right():
    # Case 6
    t = Manager().get_ticker(TEST_TICKER, start='20220101', end='20220301')
    left = t.get_path("20211201-20220120.csv")
    touch(left, content=HEADERS_CONTENT)
    right = t.get_path("20220210-20220401.csv")
    touch(right, content=HEADERS_CONTENT)
    data = t.get_raw_data()
    assert data.shape == (4, 5)
    assert t.get_files() == ["20211201-20220401.csv"]
    assert t.file.to_file() == "20211201-20220401.csv"
