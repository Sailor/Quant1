import pandas as pd
import mock

from . import SourceMock


class Mock_yfinance_Ticker(SourceMock):

    def history(self, *args, **kwargs):
        return self.function(*args, **kwargs).copy()


def mock_patch(x):
    return mock.patch('Quant1.sources.yfinance.Ticker', new=Mock_yfinance_Ticker(x))


MOCK_MONDAY14_2DAYS = pd.DataFrame(  # AAPL
    {'Open': [148.97, 152.22], 'High': [150.28, 153.59],
     'Low': [147.42999, 148.56], 'Close': [148.28, 150.03999],
     'Volume': [73374100, 89868300],
     'Dividends': [0, 0], 'Stock Splits': [0, 0]},
    index=pd.DatetimeIndex(['2022-11-14 00:00:00-05:00', '2022-11-15 00:00:00-05:00'],
                           dtype='datetime64[ns, America/New_York]', name='Date', freq=None)
)

SIMPLE_MOCK = MOCK_MONDAY14_2DAYS

MOCK_WEDNESDAY16_1DAY = pd.DataFrame(  # AAPL
    {'Open': [149.13], 'High': [149.87],
     'Low': [147.28999], 'Close': [148.78999],
     'Volume': [64218300],
     'Dividends': [0], 'Stock Splits': [0]},
    index=pd.DatetimeIndex(['2022-11-16 00:00:00-05:00'],
                           dtype='datetime64[ns, America/New_York]', name='Date', freq=None)
)


MOCK_FRIDAY18_1DAY = pd.DataFrame(  # AAPL
    {'Open': [152.31], 'High': [152.7],
     'Low': [149.97], 'Close': [151.28999],
     'Volume': [74794600],
     'Dividends': [0], 'Stock Splits': [0]},
    index=pd.DatetimeIndex(['2022-11-18 00:00:00-05:00'],
                           dtype='datetime64[ns, America/New_York]', name='Date', freq=None)
)


MOCK_MONDAY21_1DAY = pd.DataFrame(  # AAPL
    {'Open': [150.16], 'High': [150.37],
     'Low': [147.72], 'Close': [148.00999],
     'Volume': [58724100],
     'Dividends': [0], 'Stock Splits': [0]},
    index=pd.DatetimeIndex(['2022-11-21 00:00:00-05:00'],
                           dtype='datetime64[ns, America/New_York]', name='Date', freq=None)
)
