from datetime import date, timedelta
from freezegun import freeze_time
from glob import glob
import os

from ..main import LOG_LEVELS, FileName, PathName, Manager, append_file
from . import TEST_TICKER, clean_folder, touch


@clean_folder
def test_manager_basic():
    m = Manager()
    assert m.path == os.path.expanduser('~/.trading/stocks')
    t = m.get_ticker(TEST_TICKER)
    assert t.m is m
    assert t.path == os.path.expanduser('~/.trading/stocks/TEST/1D')


@clean_folder
def test_log_warning(capsys):
    m = Manager()
    assert m.log_level == 1
    t = m.get_ticker(TEST_TICKER)
    t.warning('weird ticker')
    captured = capsys.readouterr()
    assert captured.out == "WARNING: weird ticker\n"
    m = Manager(log_level=LOG_LEVELS.ERROR)
    assert m.log_level == 2
    t = m.get_ticker(TEST_TICKER)
    t.warning('weird ticker')
    captured = capsys.readouterr()
    assert captured.out == ""


@clean_folder
@freeze_time("2022-01-12")
def test_clean_dates_none_january():
    t = Manager().get_ticker(TEST_TICKER, clean_dates=False)
    assert (t.start, t.end) == (None, None)
    assert not hasattr(t, 'starts')
    t.clean_dates()
    assert (t.starts, t.ends) == ("20210101", "20220112")
    assert t.end - t.start >= timedelta(days=60)
    t = Manager(interval='1W').get_ticker(TEST_TICKER)
    assert (t.starts, t.ends) == ("20000101", "20220112")
    t = Manager(interval='1H').get_ticker(TEST_TICKER)
    assert (t.starts, t.ends) == ("20220101", "20220112")
    assert t.end - t.start >= timedelta(days=4)
    t = Manager(interval='5M').get_ticker(TEST_TICKER)
    assert (t.starts, t.ends) == ("20220112", "20220112")
    assert (t.start, t.end) == (date(2022, 1, 12), date(2022, 1, 12))


@clean_folder
@freeze_time("2022-01-04")
def test_clean_dates_none_1h_early_january():
    t = Manager(interval='1H').get_ticker(TEST_TICKER)
    assert (t.starts, t.ends) == ("20211201", "20220104")
    assert t.end - t.start >= timedelta(days=4)


@clean_folder
@freeze_time("2022-02-04")
def test_clean_dates_none_1h_early_february():
    t = Manager(interval='1H').get_ticker(TEST_TICKER)
    assert (t.starts, t.ends) == ("20220101", "20220204")
    assert t.end - t.start >= timedelta(days=4)


@clean_folder
@freeze_time("2022-02-12")
def test_clean_dates_none_february():
    t = Manager().get_ticker(TEST_TICKER)
    assert (t.starts, t.ends) == ("20210101", "20220212")
    assert t.end - t.start >= timedelta(days=60)


@clean_folder
@freeze_time("2022-03-12")
def test_clean_dates_none_other():
    t = Manager().get_ticker(TEST_TICKER)
    assert (t.starts, t.ends) == ("20220101", "20220312")


@clean_folder
@freeze_time("2022-03-12")
def test_clean_dates_with_end_date():
    t = Manager().get_ticker(TEST_TICKER, end='20220228')
    assert (t.starts, t.ends) == ("20220101", "20220228")


@clean_folder
@freeze_time("2022-03-12")
def test_get_file():
    t = Manager().get_ticker(TEST_TICKER)
    file = t.get_file('toto')
    assert file == 'toto'
    assert isinstance(file, FileName)
    path = t.get_path('toto')
    assert path == os.path.join(t.path, 'toto')
    assert isinstance(path, PathName)
    assert t.get_path() == os.path.join(t.path, "20220101-20220312.csv")
    assert t.get_path(mark=True) == os.path.join(t.path, "20220101-20220312_.csv")
    t = Manager().get_ticker(TEST_TICKER, end='20220228')
    assert t.get_path() == os.path.join(t.path, "20220101-20220228.csv")


@clean_folder
def test_append_files():
    t = Manager().get_ticker(TEST_TICKER)
    toto = t.get_path('toto')
    titi = t.get_path('titi')
    headers = "a;b;c\n"
    touch(toto, headers + "0;1;2\n")
    touch(titi, headers + "10;11;12\n")
    append_file(toto, titi)
    with open(toto) as f:
        data = f.read()
    assert data == "a;b;c\n0;1;2\n10;11;12\n"


@clean_folder
def test_append_files_refresh_tail():
    t = Manager().get_ticker(TEST_TICKER)
    toto = t.get_path('toto')
    titi = t.get_path('titi')
    headers = "a;b;c\n"
    touch(toto, headers + "0;1;2\n")
    touch(titi, headers + "10;11;12\n")
    append_file(toto, titi, f1_refresh_tail=True)
    with open(toto) as f:
        data = f.read()
    assert data == "a;b;c\n10;11;12\n"


@clean_folder
def test_append_files_no_end_cr():
    # a CR is automatically added at the end of the first file if missing
    t = Manager().get_ticker(TEST_TICKER)
    toto = t.get_path('toto')
    titi = t.get_path('titi')
    headers = "a;b;c\n"
    touch(toto, headers + "0;1;2")  # No CR
    touch(titi, headers + "10;11;12\n")
    append_file(toto, titi)
    with open(toto) as f:
        data = f.read()
    assert data == "a;b;c\n0;1;2\n10;11;12\n"


@clean_folder
def test_clean_cache_all():
    t = Manager().get_ticker(TEST_TICKER)
    files = ('toto', 'data1.csv', 'data2.csv')
    for file in files:
        touch(t.get_path(file))
    assert set(glob('*', root_dir=t.path)) == set(files)
    assert t.clean_cache(1) == 3
    assert glob('*', root_dir=t.path) == []


@clean_folder
def test_clean_cache_csv():
    t = Manager().get_ticker(TEST_TICKER)
    files = ('toto', 'data1.csv', 'data2.csv')
    for file in files:
        touch(t.get_path(file))
    assert t.clean_cache(2) == 2
    assert glob('*', root_dir=t.path) == ['toto']


@clean_folder
def test_clean_cache_other():
    t = Manager().get_ticker(TEST_TICKER, start='20220201', end='20220301')
    files = ('20211001-20211201.csv', '20220101-20220210.csv', '20220220-20220310_.csv')
    for file in files:
        touch(t.get_path(file))
    assert t.clean_cache() == 1
    assert glob('*', root_dir=t.path) == ['20220101-20220210.csv', '20220220-20220310_.csv']
