from datetime import date
import pandas as pd
import yfinance

from . import DATE_FORMAT_DASH, SEPARATOR, SPAN_1_DAY, date_f, str2date

"""
Functions that get data from providers
"""


def yf_reformat(data: pd.DataFrame, interval: str) -> pd.DataFrame:
    """
    Reformat yahoo data: remove unused columns, round floats,
    change Date format to date instead of datetime for slow intervals.
    """
    data.drop(['Dividends', 'Stock Splits'], axis=1, inplace=True)
    data = data.round(2 if data.Low.min() >= 10 else 3)
    if interval in ('1W', '1D'):
        index = [i.date().strftime(DATE_FORMAT_DASH) for i in data.index]
        data.set_index(pd.Index(index), inplace=True)
        data.index.rename('Date', inplace=True)
    return data


def yf_get_histo(ticker: str, interval: str, start: str | date, end: str | date, show=False) -> pd.DataFrame:
    """
    Get price history for ticker/interval/start_date/end_date.
    Start date and end date are both inclusive, but yfinance end date is exclusive,
    so we add one day.
    Returns a pandas dataframe.
    param 'show' is used only to help creating mocks.
    """
    start = date_f(start) if isinstance(start, str) else start.strftime(DATE_FORMAT_DASH)
    if isinstance(end, str):
        end = str2date(end)
    end = end + SPAN_1_DAY
    ticker = yfinance.Ticker(ticker)
    data = ticker.history(start=start, end=end.strftime(DATE_FORMAT_DASH), interval=interval.lower())
    if show:  # show raw data from source
        data = data.round(5)
        print({k: list(v.values()) for k, v in data.to_dict().items()})
        print(f"index=pd.{data.index}")
    return yf_reformat(data, interval)


def make_csv(data, interval, file):
    yf_reformat(data.copy(), interval).to_csv(file, sep=SEPARATOR)


sources = dict(
    yfinance=yf_get_histo
)
