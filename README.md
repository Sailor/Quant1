# Quant1

## Financial data manager

Download and localy cache financial timeseries from common providers (yahoo, ...)

This module essentially defines classes with configuration and methods to help manage historical time series
of any kind of value, stocks, options, currencies, crypto, etc...

The data are localy cached in a single folder that is configurable (eg ~/.trading).

The tree structure is of the form type/ticker/interval, where :
- `market` is one of (stock, etf, currency, crypto, ...)
- `ticker` is the symbol of the value
- `interval` specifies the resolution and is one of (1D, 1H, 5M, 1M)

Additionally:
- The root folder can contain global information or configuration, like data providers urls,
- The `market` folder can contain global information like list of tickers, gainers or loosers of the day, ...
- The `ticker` folder can contain global information, attributes (long name, ...), historical info, ....
- The `interval` folder can contain global information like history of donwloads 
  (source, date, start time, end time)

Behaviour:
- A global object is instanciated from a set of parameters (root folder, market, interval, data source, ...)
- A time series is created from a set of parameters (ticker, start date, end date)
- The time series data is fetched localy if available, else it is downloded from data source and cached localy.
- If the data is partially available localy, the missing data (and only it) is downloaded from source, then recombined
  to the local cache.
- The cached data is stored localy as a CSV file, with a name in the form start_time-end_time.csv.
- start_time and end_time format of the cache is YYYYMMDD.

Cache strategy:
The strategy adapts to the interval:
- 1W and 1D: if existent, the cache is most often extended towards the right side and the cache is mostly reused
  in the future. Mostly there is only one cache spanning a long-time period (1 year or more).
- 1H, 5M and 1M: there are many small files and only the most recent is used.
  Mostly, each cache spans a small-time period (1 day or 1 week). There may be a cleaning policy at work here.
