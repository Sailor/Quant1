from glob import glob, iglob
from itertools import pairwise
import pandas as pd
from typing import List, Tuple

from . import *
from .sources import sources


# TODO try Arrow/Parquet as a data manager


def merge_csv(left: PathName, right: PathName):
    """
    Append right CSV file to left CSV file, jumping right file headers first,
    then remove right file
    """
    append_file(left, right)
    os.remove(right)


class Manager:
    """
    This class stores the root/market/interval/source attributes and manages the local cache pathes.
    A source is not required if you know for sure that your data is in cache.
    """

    def __init__(self, root=ROOT_FOLDER, market='stocks', interval='1D', source=None, log_level=LOG_LEVELS.WARNING):
        assert market in MARKETS
        assert interval in INTERVALS
        self.root = root
        self.market = market
        self.interval = interval
        self.source = source or DEFAULT_SOURCE
        assert self.source in sources
        self.log_level = log_level
        self.count_warning = COUNT_FILE_WARNING
        self.init_path()

    def init_path(self):
        self.path = os.path.join(self.root, self.market)
        os.makedirs(self.path, exist_ok=True)

    def get_ticker(self, ticker: str, start=None, end=None, **kwargs):
        """ Get the ticker object for a start and end date
        :param ticker: ticker name
        :param start: (date|str|None) the start date for the time series
        :param end: (date|str|None) the end date for the time series
        :return: A Ticker object
        """
        path = os.path.join(self.path, ticker, self.interval)
        os.makedirs(path, exist_ok=True)
        return Ticker(path, ticker, self, start, end, **kwargs)

    def warning(self, message):
        if self.log_level <= LOG_LEVELS.WARNING:
            print(f"WARNING: {message}")


class Ticker:
    """
    This class retrieves and stores the time series data for the specified ticker, interval and time span.
    Whatever the interval, the start and end dates of the time span are always dates (no datetime).
    But, inside the CSV file (and the DataFrame) the time index is a date for intervals (1W, 1D) and
    a datetime for smaller intervals.
    Start and end dates are inclusives, i.e. a file containing data for a single day will be named
    e.g. 20221101-20221101.csv (Tuesday nov 1st, 2022).
    """

    def __init__(self, path: str, ticker: str, manager: Manager, start, end, **kwargs):
        assert path and ticker
        self.path = path
        self.ticker = ticker
        self.m = manager
        self.warning = self.m.warning
        self.start, self.end = start, end
        self.end_is_today = False
        self.file = None  # store effective file name of the data, that can be different from start-end.csv
        self.cache = True
        if kwargs.get('clean_dates', True):
            self.clean_dates()

    def clean_dates(self) -> None:
        """
        Clean self.start and self.end dates.
        If self.end is None, it is replaced by now().
        If self.start is None, it is replaced by start of year, month or now(), depending on interval.
        self.start and self.end are always UTC dates, no datetime.
        """
        today = datetime.utcnow().date()
        start, end = self.start, self.end or today
        end_is_str = isinstance(end, str)
        _end = None
        interval = self.m.interval
        if not start:
            _end = str2date(end) if end_is_str else end
            now = datetime.utcnow().date()
            if interval == '1W':
                start = GLOBAL_START
            elif interval == '1D':
                start = now.replace(month=1, day=1)
                if _end - start < timedelta(days=MIN_INTERVAL_1D):
                    start = start.replace(year=start.year - 1)
            elif interval == '1H':
                start = now.replace(day=1)
                if _end - start < timedelta(days=MIN_INTERVAL_1H):
                    month = (start.month - 1) or 12
                    year = start.year - int(month == 12)
                    start = start.replace(year=year, month=month)
            else:
                start = end
        if isinstance(start, str):
            _start = datetime.strptime(start, DATE_FORMAT).date()
        else:
            _start = start
            start = start.strftime(DATE_FORMAT)
        if end_is_str:
            _end = _end or datetime.strptime(end, DATE_FORMAT).date()
        else:
            _end = end
            end = end.strftime(DATE_FORMAT)
        assert start <= end
        assert _end <= today
        self.starts, self.ends = start, end  # string
        self.start, self.end = _start, _end  # date
        self.end_is_today = _end == today

    def get_file(self, file: str = None, start: str = None, end: str = None, mark=False) -> FileName:
        """ Creates filename from optional parameters or self.start, self.end
        """
        if not file:
            start = start or self.starts
            end = end or self.ends
            if mark:
                file = f"{start}-{end}_.{EXTENSION}"
            else:
                file = f"{start}-{end}.{EXTENSION}"
        return FileName(file)

    def get_path(self, file: str = None, start=None, end=None, mark=False) -> PathName:
        file = self.get_file(file, start, end, mark)
        return file.to_path(self.path)

    def get_files(self) -> List[str]:
        """ Get list of files in the ticker's cache (from filesystem)
        """
        return sorted(iglob(f'*.{EXTENSION}', root_dir=self.path))

    def read_file(self, file=None) -> pd.DataFrame:
        return pd.read_csv(self.get_path(file or self.file), sep=SEPARATOR, index_col='Date')

    def get_raw_data(self) -> pd.DataFrame:
        """
        Gets data from source and/or cache, creates cache from source or merges source data to existing cache.
        Returns aggregated data from file(s) and source.
        """
        if not self.cache:
            self.filter = False
            return self.get_from_source(self.start, self.end)
        new_file = None
        for file, start, end, mode in self.get_from_cache():
            if start and end:  # main cases
                _file = file or new_file
                if _file and not is_refresh_end(_file):
                    start = next_work_date(start)
                data = self.get_from_source(start, end)
                new_file, data = self.cache_data(_file, data, mode)
            elif mode == 'p':  # case 6, 2nd file
                new_file = self.prepend_csv2csv(new_file, file)
                data = self.read_file(new_file)
            else:  # case 2
                data = self.read_file(file)
        self.file = new_file or self.get_path(file)  # effective file
        return data

    def get_data(self):
        """
        Main method
        Returns effective data for the time span.
        """
        data = self.get_raw_data()
        if self.filter:
            start = date_f(self.starts)
            end = date_f(self.ends)
            return data[(data.index >= start) & (data.index <= end)]
        return data

    def get_from_cache(self) -> List[Tuple]:
        """
        Calculates parameters for data retrieval from cache and/or source
        """
        files = self.get_files()
        # There are 6 different cases
        self.filter = True
        if not files:
            # Case 1
            # Empty folder, create first file
            self.filter = False
            return [(None, self.starts, self.ends, '')]
        num_files = len(files)
        if num_files >= self.m.count_warning:
            self.warning(f"{self.path} contains {num_files} {EXTENSION} files")
        # itertools.pairwise('ABCD') --> AB BC CD
        for f1, f2 in pairwise(files + [None]):
            start, end = get_dates(f1, clean=True)
            start_ok = start <= self.starts <= end
            end_ok = start <= self.ends <= end
            if start_ok and end_ok:
                # Case 2
                # Requested time span entirely contained in file
                return [(f1, None, None, '')]
            if start_ok:
                # Case 3
                # Left side of time span found in file, append source later
                res = [(f1, end, self.ends, 'a')]
                if f2:
                    start2, end2 = get_dates(f2, clean=True)
                    if start2 <= self.ends <= end2:
                        # Case 6
                        # Left side of time span found in first file, right side in second file
                        # Join them with data from source in the middle
                        res = [(f1, end, start2, 'a'), (f2, None, None, 'p')]
                return res
            if end_ok:
                # Case 4
                # right side of time span found in file, prepend source later
                return [(f1, self.starts, start, 'p')]
            if self.starts < start <= end < self.ends:
                # Case 5
                # Cache strictly inside the time span
                self.filter = False
                return [(f1, self.starts, start, 'p'), (None, end, self.ends, 'a')]
        else:
            # Case 1 bis
            # No file intersects with requested time span, create a new one
            self.filter = False
            return [(None, self.starts, self.ends, '')]

    def get_from_source(self, start, end, source=None) -> pd.DataFrame:
        source = source or self.m.source
        assert source
        data = sources[source](self.ticker, self.m.interval, start, end)
        return data

    def create_csv(self, data: pd.DataFrame) -> PathName:
        file = self.get_path(mark=self.end_is_today)
        data.to_csv(file, sep=SEPARATOR)
        return file

    def append_csv(self, file: str, data: pd.DataFrame) -> PathName:
        """ Append data to an existing CSV file, changing its name according to the new end date
        """
        start, _ = get_dates(file)
        origin = self.get_path(file)
        if origin.is_refresh_end():
            truncate_tail(origin)
        data.to_csv(origin, mode='a', header=False, sep=SEPARATOR)
        dest = self.get_path(start=start, end=self.ends, mark=self.end_is_today)
        os.rename(origin, dest)
        return dest

    def prepend_data2csv(self, file: str, data: pd.DataFrame) -> PathName:
        """ Prepend data to an existing CSV file, changing its name according to the new start date
        """
        _, end = get_dates(file)
        new_file = self.get_path(start=self.starts, end=end)
        data.to_csv(new_file, sep=SEPARATOR)
        old_file = self.get_path(file)
        merge_csv(new_file, old_file)
        return new_file

    def prepend_csv2csv(self, left: PathName, right: str) -> PathName:
        start, _ = get_dates(left)
        _, end = get_dates(right)
        merge_csv(left, self.get_path(right))
        new_file = self.get_path(start=start, end=end)
        os.rename(left, new_file)
        return new_file

    def cache_data(self,
                   file: None | str | PathName,
                   data: pd.DataFrame,
                   mode: str) -> Tuple[PathName, pd.DataFrame]:
        if not file:
            new_file = self.create_csv(data)
            return new_file, data
        if mode == 'a':
            new_file = self.append_csv(file, data)
            return new_file, self.read_file(new_file)
        if mode == 'p':
            new_file = self.prepend_data2csv(file, data)
            return new_file, self.read_file(new_file)

    def clean_cache(self, target=CLEAN_TARGETS.OTHER) -> int:
        # TODO there are other cases than the 6 treated in this class
        # TODO to keep coherency, we should clean or merge any file that intersects the resulting cache file
        """
        Clean the self.path directory:
        - if target is ALL, remove all files
        - if target is CACHE, remove all cache files
        - if target is OTHER, remove all cache files except the current one
        :param target: one of (ALL, CACHE, OTHER)
        :return: the number of files removed
        """
        if target == CLEAN_TARGETS.ALL:
            files = glob('*', root_dir=self.path)
        elif target == CLEAN_TARGETS.CACHE:
            files = glob(f'*.{EXTENSION}', root_dir=self.path)
        elif target == CLEAN_TARGETS.OTHER:
            files = set(iglob(f'*.{EXTENSION}', root_dir=self.path)) - \
                    set(f for f, _, _, _ in self.get_from_cache() if f)
        for file in files:
            os.remove(os.path.join(self.path, file))
        return len(files)
